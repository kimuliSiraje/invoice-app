# Invoice App

## Project setup

```
git clone https://gitlab.com/kimuliSiraje/invoice-app.git
cd invoice-app
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
You should have configured server;
[visit vue-route docs for guidance](https://router.vuejs.org/guide/essentials/history-mode.html).
Use the dist file in production.
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```