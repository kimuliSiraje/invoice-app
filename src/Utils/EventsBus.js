import Vue from 'vue'

window.events = new Vue

window.loader = {
    start: () => window.events.$emit('start-loader'),
    stop: () => window.events.$emit('stop-loader'),
}
