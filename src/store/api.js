import axios from 'axios'

axios.defaults.baseURL = 'https://5c54d8c984df580014cd0689.mockapi.io/sp5/api/v1'

axios.interceptors.request.use(config => {
    window.loader.start()
    return config
})

axios.interceptors.response.use(response => {
    window.loader.stop()
    return response
})

export default {

    //customers
    fetchCustomers(success, failed) {
        axios.get(`/customer?sortBy=createdAt`).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    destroyCustomer(customerId, success, failed) {
        axios.delete(`/customer/${customerId}`).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    storeCustomer(customerData, success, failed) {
        axios.post('/customer', customerData).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    patchCustomer(customer, success, failed) {
        axios.put(`/customer/${customer.id}`, customer).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    //products
    fetchProducts(success, failed) {
        axios.get(`/product`).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    destroyProduct(productId, success, failed) {
        axios.delete(`/product/${productId}`).
            then(() => success()).
            catch(error => failed(error))
    },

    storeProduct(productData, success, failed) {
        axios.post('/product', productData).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    patchProduct(product, success, failed) {
        axios.put(`/product/${product.id}`, product).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    //invoices
    fetchInvoices(customerId, success, failed) {
        axios.get(`/customer/${customerId}/invoice?sortBy=createdAt`).
            then(({data}) => success(data)).
            catch(error => failed(error))
    },

    destroyInvoice(data, success, failed) {
        axios.delete(`/customer/${data.customerId}/invoice/${data.invoiceId}`).
            then(() => success()).
            catch(error => failed(error))
    },

}