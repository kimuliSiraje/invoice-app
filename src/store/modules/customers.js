import api from '@/store/api'

const state = {
    count: 0,
    all: [],
}

const getters = {
    findCustomer: (state) => (customerId) => {
        return state.all.find(customer => customer.id === customerId)
    },
}

const actions = {

    getAllCustomers({commit}) {
        return new Promise((resolve, reject) => {
            api.fetchCustomers(customers => {
                commit('SET_CUSTOMERS', customers.items)
                resolve(customers.items)
            }, error => reject(error))
        })
    },


    deleteCustomer({commit}, customerId) {
        api.destroyCustomer(customerId, () => {
            commit('REMOVE_CUSTOMER', customerId)
            window.events.$emit('customer-deleted')
        })
    },

    addCustomer({commit}, customerData) {
        api.storeCustomer(customerData, (data) => {
            data.invoices = []
            commit('SET_CUSTOMER', data)
            window.events.$emit('customer-created', data)
        })
    },

    updateCustomer({commit}, customerData) {
        api.patchCustomer(customerData, (data) => {
            commit('UPDATE_CUSTOMER', data)
            window.events.$emit('customer-updated', data)
        })
    },

}

const mutations = {
    SET_CUSTOMERS(state, customers) {
        state.all = customers
    },

    SET_CUSTOMER(state, customer) {
        state.all.unshift(customer)
    },

    UPDATE_CUSTOMER(state, data) {
        let foundCustomer = state.all.find(customer => customer.id === data.id)
        for (let field in data) {
            foundCustomer[field] = data[field]
        }
    },

    REMOVE_CUSTOMER(state, customerId) {
        state.all = state.all.filter(customer => customer.id !== customerId)
    },

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
