import api from '@/store/api'

const state = {
    count: 0,
    all: [],
}

const getters = {
    findProduct: (state) => (productId) => {
        return state.all.find(product => product.id === productId)
    },
}

const actions = {
    getAllProducts({commit}) {
        api.fetchProducts(products => {
            commit('SET_PRODUCTS', products)
            commit('SET_PRODUCTS_COUNT', products.length)
        })
    },

    addProduct({commit}, productData) {
        api.storeProduct(productData, (data) => {
            commit('SET_PRODUCT', data)
            window.events.$emit('product-created')
        })
    },
    updateProduct({commit}, productData) {
        api.patchProduct(productData, (data) => {
            commit('UPDATE_PRODUCT', data)
            window.events.$emit('product-updated', data)
        })
    },

    deleteProduct({commit}, productId) {
        api.destroyProduct(productId, () => {
            commit('REMOVE_PRODUCT', productId)
            window.events.$emit('product-deleted')
        })
    },
}

const mutations = {
    SET_PRODUCTS(state, products) {
        state.all = products
    },
    SET_PRODUCT(state, product) {
        state.all.unshift(product)
    },
    SET_PRODUCTS_COUNT(state, count) {
        state.count = count
    },
    UPDATE_PRODUCT(state, data) {
        let foundProduct = state.all.find(product => product.id === data.id)
        for (let field in data) {
            foundProduct[field] = data[field]
        }
    },
    REMOVE_PRODUCT(state, productId) {
        state.all = state.all.filter(product => product.id !== productId)
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
