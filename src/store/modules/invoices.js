import moment from 'moment'
import api from '@/store/api'

const state = {
    count: 0,
    all: [],
}

const getters = {
    latestInvoices: (state) => {
        let invoices = state.all.slice(0, 4)
        return invoices.sort((a, b) => {
            return moment(a.createdAt).isAfter(b.createdAt)
        })
    },

    findInvoice: (state) => (invoiceId) => {
        return state.all.find(invoice => invoice.id === invoiceId)
    },

    totalPrice: (state) => (invo) => {
        return state.all.
            find(invoice => invoice.id === invo.id).
            products.
            reduce((price, product) => {
                return parseFloat(price + product.item_price).
                    toFixed(2)
            }, 0)
    },
}

const actions = {
    getAllInvoices({commit, rootState}) {
        rootState.customers.all.forEach(customer => {
            api.fetchInvoices(customer.id, (data) => {
                commit('ADD_INVOICES', data)
            })
        })
    },
    deleteInvoice({commit}, invoice) {
        api.destroyInvoice({
            invoiceId: invoice.id,
            customerId: invoice.customerId,
        }, () => {
            commit('REMOVE_INVOICE', invoice.id)
            window.events.$emit('invoice-deleted', invoice)
        })
    },
}

const mutations = {
    SET_INVOICES(state, invoices) {
        state.all = invoices
    },

    ADD_INVOICES(state, invoices) {
        invoices.forEach(invoice => state.all.push(invoice))
    },

    REMOVE_INVOICE(state, invoiceId) {
        state.all = state.all.filter(invoice => invoice.id !== invoiceId)
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
