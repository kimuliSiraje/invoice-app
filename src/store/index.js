import Vue from 'vue'
import Vuex from 'vuex'

import invoices from './modules/invoices'
import customers from './modules/customers'
import products from './modules/products'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
    modules: {
        invoices,
        customers,
        products,
    },
    strict: debug,
})
