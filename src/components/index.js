export const TheHeader = () => import('./Navigation/TheHeader')
export const Invoice = () => import('./Invoice')
export const Customer = () => import('./Customer')
export const Product = () => import('./Product')
export const UiLoader = () => import('./Ui/Loader')
export const Paginate = () => import('./Ui/Pagination')
