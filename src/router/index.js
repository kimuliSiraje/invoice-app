import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import report from './report'
import customers from './customers'
import invoices from './invoices'
import products from './products'

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        ...report,
        ...customers,
        ...invoices,
        ...products,
    ],
})

export default router
