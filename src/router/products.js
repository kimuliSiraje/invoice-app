export default [
    {
        path: '/products',
        name: 'products',
        component: () => import('@/views/products/Index'),
    },
    {
        path: '/products/:id',
        name: 'products-show',
        component: () => import('@/views/products/Show'),
    },
    {
        path: '/products/:id/edit',
        name: 'products-edit',
        component: () => import('@/views/products/Edit'),
    },
    {
        path: '/products/create',
        name: 'products-create',
        component: () => import('@/views/products/Create'),
    },
]