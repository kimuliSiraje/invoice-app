export default [
    {
        path: '/customers',
        name: 'customers',
        component: () => import('@/views/customers/Index'),
    },
    {
        path: '/customers/create',
        name: 'customers-create',
        component: () => import('@/views/customers/Create'),
    },
    {
        path: '/customers/:id',
        name: 'customers-show',
        component: () => import('@/views/customers/Show'),
    },
    {
        path: '/customers/:id/edit',
        name: 'customers-edit',
        component: () => import('@/views/customers/Edit'),
    },
]