export default [
    {
        path: '/invoices',
        name: 'invoices',
        component: () => import('@/views/invoices/Index'),
    },
    {
        path: '/invoices/:id',
        name: 'invoices-show',
        component: () => import('@/views/invoices/Show'),
    },{
        path: '/invoices/:id/edit',
        name: 'invoices-edit',
        component: () => import('@/views/invoices/Edit'),
    },
]