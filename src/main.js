import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toasted from 'vue-toasted'
import './assets/less/main.less'

require('@/Utils')

Vue.config.productionTip = false
Vue.use(Toasted, {
    theme: 'toasted-primary',
    position: 'bottom-right',
    duration: 2000,
})

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')
