import {shallowMount, RouterLinkStub} from '@vue/test-utils'
import Invoice from '@/components/Invoice'

describe('Invoice Component', () => {
    const invoice = {
        'id': '1',
        'customerId': '15',
        'createdAt': '2019-02-24T04:38:31.270Z',
        'invoice_no': 64161,
        'due_date': '2019-05-09T17:59:35.611Z',
        'products': [],
    }
    const wrapper = shallowMount(Invoice, {
        stubs: {RouterLink: RouterLinkStub},
        propsData: {invoice: invoice},
    })

    it('renders prop.invoice when passed', () => {
        expect(wrapper.text()).toContain('64161')
    })

    it('renders link to show a invoice', () => {
        expect(wrapper.find('a').props().to.name).toEqual('invoices-show')
    })

    it('renders link to edit a invoice', () => {
        expect(wrapper.find('a').props().to.name).toEqual('invoices-edit')
    })
})
