import {shallowMount, RouterLinkStub} from '@vue/test-utils'
import Customer from '@/components/Customer'

describe('Customer Component', () => {
    const customer = {
        'id': '15',
        'createdAt': '2019-02-24T09:01:27.240Z',
        'name': 'Wiegand Inc',
        'avatar': 'https://s3.amazonaws.com/uifaces/faces/twitter/jackiesaik/128.jpg',
        'address': '64826-1915',
        'invoices': [],
    }
    const wrapper = shallowMount(Customer, {
        stubs: {RouterLink: RouterLinkStub,},
        propsData: {customer: customer},
    })

    it('renders prop.customer when passed', () => {
        expect(wrapper.text()).toContain('Wiegand Inc')
        expect(wrapper.text()).toContain('64826-1915')
    })

    it('renders link to show a customer', () => {
        expect(wrapper.find('a').props().to.name).toEqual('customers-show')
    })
})
