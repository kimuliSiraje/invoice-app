import {shallowMount, RouterLinkStub} from '@vue/test-utils'
import Product from '@/components/Product'

describe('Product Component', () => {
    const product = {
        "id": "22",
        "createdAt": "2019-02-24T05:54:40.641Z",
        "product_name": "Licensed",
        "product_price": "28.00",
        "img": "https://s3.amazonaws.com/uifaces/faces/twitter/damenleeturks/128.jpg"
    }
    const wrapper = shallowMount(Product, {
        stubs: {
            RouterLink: RouterLinkStub,
        },
        propsData: {product: product},
    })

    it('renders prop.product when passed', () => {
        expect(wrapper.text()).toContain('Licensed')
    })

    it('renders link to show a product', () => {
        expect(wrapper.find('a').props().to.name).toEqual('products-edit')
    })
})
